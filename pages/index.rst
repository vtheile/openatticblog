.. title: Open Source Ceph and Storage Management
.. slug: index
.. date: 2016-11-19 10:15:28 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: openATTIC.org index page
.. type: text
.. pretty_url: False
.. nocomments: True


openATTIC is an Open Source Ceph and storage management solution for Linux, with
a strong focus on storage management in a datacenter environment.

The project is sponsored by `SUSE <https://suse.com/>`_. It is licensed under
the GNU General Public License (GPLv2) and follows a completely :doc:`open and
inclusive development process <get-involved>`.

* :doc:`Download <download>`

Unified Storage solution
------------------------

openATTIC supports a wide range of storage technologies, both file- and
block-based, e.g. NFS, CIFS, iSCSI and FibreChannel. It is also capable of
managing multiple nodes from a single web interface.

If your storage requirements exceed the boundaries of individual servers,
openATTIC also supports the distributed storage system `Ceph
<http://ceph.com>`_.

Software-defined Storage
------------------------

All functionality is available via a clean and intuitive web-based user
interface and RESTful API through a common backend.

See the :doc:`feature list <features>` for details or take a look at our `online
demo <https://demo.openattic.org/openattic/>`_.

:doc:`Installation packages <download>` are available in the form of apt and yum
repositories for Ubuntu/Debian as well as Red Hat Enterprise Linux/CentOS and
SUSE Linux.
