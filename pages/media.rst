.. title: Media
.. slug: media
.. date: 2016-12-05 10:15:28 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: openATTIC.org screen shots and videos
.. type: text
.. pretty_url: False
.. nocomments: True


Screen Shots
------------

.. slides::

    /galleries/oA-Screenshots/openATTIC-Storage_Dashboard.png
    /galleries/oA-Screenshots/openATTIC-Volume_Management.png
    /galleries/oA-Screenshots/openATTIC-API_Recorder.png
    /galleries/oA-Screenshots/openATTIC-Ceph_CRUSH_Map.png
    /galleries/oA-Screenshots/openATTIC-Ceph_Dashboard.png
    /galleries/oA-Screenshots/openATTIC-Ceph_OSD_List.png
    /galleries/oA-Screenshots/openATTIC-Ceph_Pool_Creation.png
    /galleries/oA-Screenshots/openATTIC-Ceph_Pool_List.png
    /galleries/oA-Screenshots/openATTIC-Ceph_RBD_Creation.png
    /galleries/oA-Screenshots/openATTIC-Ceph_RBD_List.png


Videos
------

You can find videos of presentations about openATTIC as well as screen casts
on the `openATTIC YouTube Channel <https://www.youtube.com/user/openATTICManagement>`_

.. youtube:: Ooh-tjSVmNs