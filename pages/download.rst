.. title: Download
.. slug: download
.. date: 2016-12-03 10:21:31 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

Installing openATTIC
--------------------

openATTIC can be installed on a variety of Linux distributions.

There is no individual download of software - we provide dedicated APT and yum
repositories to facilitate the easy installation and automatic updates for
Debian/Ubuntu as well as RPM-based distributions like Red Hat Enterprise
Linux/CentOS (and other RHEL derivatives).

Packages for SUSE Linux (openSUSE Leap and SLES 12) can be obtained from
the `openSUSE Build Service <http://software.opensuse.org/package/openattic>`_.

See the `openATTIC Installation Guide
<http://docs.openattic.org/2.0/install_guides/>`_ for instructions on how to
subscribe to these repositories in order to install these packages using your
distribution's package management utilities.