.. title: One Year in the openATTIC Team: A Summary
.. slug: one-year-in-the-openattic-team-a-summary
.. date: 2016-08-05 11:35:28 UTC+02:00
.. tags: community, contributing, development, collaboration, opensource
.. category:
.. link:
.. description: Summarizing some highlights and key accomplishments in the past year
.. type: text
.. author: Lenz Grimmer

A month ago, I concluded my first year in the openATTIC team. How time flies
when you're having fun!

One of my goals early on was to make openATTIC more open and accessible for
community contributors and early adopters. There were a few barriers that had
to be removed for this to happen.

In this post, I'd like to recapitulate some noteworthy changes that took place
in the openATTIC project during the last 12 months. I also would like to
summarize some highlights and achievements.

I realized that I speak about many of these when I give presentations or talk
with people about openATTIC, but I think it makes sense to also put them in
writing in this blog.

.. TEASER_END

Removing Barriers and Refining Processes
----------------------------------------

One big step was that we removed the "Enterprise/Community Edition" split and
published the entire openATTIC code base under the GPLv2 license exclusively.
There is no more dual licensing funkyness.

We also dropped the requirement for submitting a contributor license agreement
(CLA), in order to lower the barrier for new developers to submit patches to
us. Today, all that is needed is to `sign off your submitted patches
<http://docs.openattic.org/2.0/developer_docs/contributing_guidelines.html#signing-your-patch-contribution>`_,
similar to how the Linux kernel and many other project do it.

This has also changed our business model: instead of "selling software", we'll
now focus on delivering services and support around it. This part is still
under development, but the plan is to provide the usual offerings in the form
of support and software subscription plans. We'll share more details about
this when we've finalized our planning here, but if you have some ideas and
expectations around this, we'd like to hear from you!

We also changed many processes and ways in which we worked on the code base,
to accelerate and improve the development.

For example, we consolidated code of various components like documentation,
test suites and the former "Enterprise" features into `a single Mercurial
repository <https://bitbucket.org/openattic/openattic/>`_, to simplify the
development and release process. This makes it easier to submit changes that
affect all these components in an "atomic" fashion (e.g. when committing a new
feature implementation including the related tests and documentation).

Furthermore, we started making more effective use of Mercurial branches. We
switched to using a stable/development branch model and having a Jenkins
instance as the "gatekeeper": only if all tests have passed on the development
branch, a merge into the default branch is performed automatically.

This ensures that we can theoretically cut a new release from the default
branch at any time, with high confidence. The stable branch is also used for
building our nightly snapshot builds and is the code base used on our `live
demo <https://demo.openattic.org/>`_, which gives us additional test coverage.

To reduce the turnaround time for bug fixes and for new features to be
available shortly after they have been committed, we also switched to a
monthly release schedule. This means that if some feature did not make it in
time for a release, another one is always around the corner, and a developer
does not have to wait for months to get his work incorporated.

We also :doc:`opened our development processes
<opening-up>`: all of our work (bugs, new
features, enhancements) is now tracked in a public `Jira
<https://tracker.openattic.org>`_ instance. Changes to the code base are
proposed in the form of `public pull requests and code reviews
<https://bitbucket.org/openattic/openattic/pull-requests/>`_.

Infrastructure Improvements
---------------------------

In addition to moving the formerly internal Jira instance to the public, we
further extended our project infrastructure by also adding a public `Mumble
server <https://blog.openattic.org/posts/openattic-mumble-server/>`_ as well
as an :doc:`Etherpad <etherpad-for-openatticorg>` for collaboration purposes.
We also established a `Google Group
<https://groups.google.com/forum/#!forum/openattic-users>`_ so our users can
get in touch with us, to discuss existing features as well as new
functionality.

The openATTIC blog was :doc:`moved <why-we-chose-nikola>` from a shared
WordPress blog to a dedicated instance based on the `Nikola
<https://getnicola.com>`_ static site generator. The motivation was to lower
the bar for our developers to generate content without having to leave their
usual work environment. Posting a new blog post now only requires creating a
text file in reStructuredText markup (the same we use for the openATTIC
documentation), commit and push the change and trigger a Jenkins job to
rebuild the site.

We attended and participated in numerous `events and conferences
<link://tag/event>`_, to spread the word about openATTIC.

Community Reception
-------------------

These activities in opening up and to foster a community around openATTIC have
already resulted in some notable activities.

For example, we :doc:`established a development cooperation
<collaborating-with-suse>` with `SUSE
<https://www.suse.com/communities/blog/ceph-and-storage-management-on-suse-with-openattic/>`_,
to help accelerating the Ceph management and monitoring capabilities within
openATTIC and to port openATTIC to the SUSE Linux platform. We also had joint
presentations at conferences and webinars with them and visited the :doc:`SUSE
Hackweek <summary-of-suse-hackweek-activities>`.

We have started initial discussions with some hardware vendors, to discuss the
bundling of their systems with openATTIC in the form of pre-installed
appliances. We're in the early stages here, but it's exciting to have these
conversations and to learn more about the requirements and issues raised by
these partners.

A group of four students from Pune (India) will perform development work on
openATTIC as part of a research project for their final year of college.  They
are currently working on setting up their development environments and getting
familiar with the code base, and we look forward to their first contributions.

A group of developers in Malaysia has expressed interest in the Ceph
management part of openATTIC and is currently testing it on their production
site. Moreover, they are looking into developing an alternative UI prototype
for the Ceph management, it will be interesting to see what they come up with.

In general, traffic on the #openattic IRC channel and our Google Group has
picked up noticeably and we're also receiving first code contributions from
external developers.

We've actually created a dedicated page that lists `low hanging fruit tasks
<https://wiki.openattic.org/display/OP/Low+hanging+fruit+tasks>`_ that should
be suitable for new developers to get started.

I think we're heading in the right direction with the changes and improvements
that we've implemented. We're still in the early stages, but the trend
indicators look promising. We look forward to working with a growing community
of users and developers!
