.. title: Q&A and recording from yesterday's SUSE Monthly Open Source Talks Webinar
.. slug: qa-and-recording-from-yesterdays-suse-monthly-open-source-talks-webinar
.. date: 2016-07-07 18:01:04 UTC+02:00
.. tags: ceph, community, contributing, development, webinar, jira, suse
.. category:
.. link: 
.. description: Answering question from yesterday's SUSE MOST webinar
.. type: text
.. author: Lenz Grimmer

Yesterday, I gave a webinar about managing Ceph and Storage with openATTIC for
the `SUSE Monthly Open Source Talks <https://www.suse.com/partners/most>`_. In
case you missed it, a recording of the session is now available from the "on
demand" section of that web site.

I really enjoyed giving this presentation and sharing an update about the
current state of development with the audience. In fact, I got somewhat
carried away, so I nearly ran out of time! Unfortunately this left no time for
doing Q&A after the presentation, I apologize for that. However, we took note
of all your questions, which I will answer below.

.. TEASER_END

**Will a link to the RPM builds for SUSE be added to the openATTIC download
page?**

Very good suggestion, thanks! The installation on SUSE was already documented
in the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_, we have now added
a link to the SUSE repos to the download page as well.

**Besides software package downloads, is there a software (VM) appliance in
the works and what requirements might there be for such an evaluation?**

Not at this time, sorry. We have an open issue for that (:issue:`OP-505`), but
have not yet gotten around to implement this. If you don't want to perform an
installation, you can use our `live demo <https://demo.openattic.org/>`_ for
getting a first impression (it will be refreshed every top of the hour).

Another idea would be to provide a docker image (for a pure Ceph management
instance of openATTIC) - we're open for suggestions or contributions here!

When it comes to requirements, this depends heavily on your intended use case.
If you want to evaluate the storage management functionality, your test system
should have at least one dedicated disk drive for data. If you're just
interested in the Ceph management part, you don't actually need an additional
disk. Other system requirements are outlined `in the documentation
<http://docs.openattic.org/2.0/install_guides/oA_installation.html>`_.

**Is openATTIC modular enough to be used to manage/monitor various storage
technologies (NAS/SAN/Ceph) from a single deployed instance?**

Yes, it is. In fact you can manage multiple openATTIC instances (and Ceph
clusters) from the same WebUI. The only requirement is that all openATTIC
instances have shared access to the PostgreSQL database that hosts the
openATTIC runtime data.

**Are there any plans to integrate with hardware management such as
libstoragemgmt?**

We're evaluating if it makes sense to replace our custom storage management
code with calls to `libstoragemgmt
<https://libstorage.github.io/libstoragemgmt-doc/>`_, this is tracked in
:issue:`OP-1155`. One concern is that this framework needs to be available and
fully supported on all the distributions we support, this requires further
evaluation/investigation.

**Can you elaborate on the proposed "Salt integrations" going forward?**

As a first step, we intend to start using Salt Open for the remote management
and automation of tasks related to managing Ceph nodes. In particular, we will
create support for SUSE's `Salt-based Ceph management framework
<https://github.com/swiftgist/pillar-prototype>`_ that is currently in the
works. This work and related subtasks is tracked in issue :issue:`OP-1243` in
our public Jira. However, once the basic infrastructure support for Salt is in
place, we could also use it for performing other storage-related management
tasks, for example as a replacement for the openATTIC systemd process.

**Any plans for OSD and PG troubleshooting? I am thinking even as much sane
configuration suggestions based on errors and warning from ceph status.**

That would indeed be a good idea! As a first step, we will start with giving
the administrator more insight into various performance and runtime
information via monitoring and graphing. Once we can keep track of this
information, we're in a better position to analyze it and come up with
warnings and suggestions on how to fix potential issues based on patterns. In
addition to that, we're thinking of adding an editor similar to the `PG
calculator <http://ceph.com/pgcalc/>`_ to generate sane PG numbers for all
pools (see :issue:`OP-1072` for details).

**Is CephFS support on the roadmap?**

It certainly is - this work is tracked in issue :issue:`OP-925` and its
subtasks. What CephFS management features would you most  be interested in?
Please let us know.

**Will openATTIC allow for management of individual devices (start, stop, in,
out, create, destroy)?**

If you're referring to Ceph OSDs here, then yes - this depends on the
Salt-based management framework to be complete and is tracked via the subtasks
of :issue:`OP-1004`.

**Will openATTIC make it easier to manage lrbd configurations and include such
concepts as LUN masking?**

Making it easier to manage lrbd configurations is one of the goals we just
discussed with the SUSE developers during last week's hackweek. Again we'll
likely be using Salt here, the integration of lrdb support is tracked in
:issue:`OP-775`.

**Will the nagios/icinga integration also monitor the throughput of the
various networks (public/cluster) and individual interfaces?**

As a first step, we'll add monitoring for all resources that can be queried
via librados locally. In a next step, we plan to use Salt to deploy and
configure `collectd <http://collectd.org/>`_ on all Ceph nodes, to collect
this kind of node-specific performance data. See :issue:`OP-1244` for details.

**Is there support for a Ceph cluster configuration report to identify
differences in node configurations?**

Not that I am aware of - would you mind elaborating on your idea by submitting
an issue in our `public Jira  tracker <https://tracker.openattic.org/>`_ for
this, or by contacting us via `IRC or the Google Group
<http://openattic.org/get-involved.html>`_?

Thank you!
