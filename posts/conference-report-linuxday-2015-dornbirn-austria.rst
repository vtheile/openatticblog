.. title: Conference report: LinuxDay 2015 Dornbirn, Austria
.. slug: conference-report-linuxday-2015-dornbirn-austria
.. date: 2015-11-25 16:20:27 UTC+01:00
.. tags: conference, event, linux, dornbirn, openattic, storage
.. category: 
.. link: 
.. description: A summary of my visit to LinuxDay 2015 in Dornbirn, Austria
.. type: text
.. author: Lenz Grimmer

.. thumbnail:: ../../images/linuxday-dornbirn.jpg

On November 21st, the `Linux User Group Vorarlberg <http://www.lugv.at/>`_
organized the 17th `LinuxDay <http://linuxday.at/>`_ in Dornbirn, Austria. It
took place at the `Höhere Technischen Lehranstalt (HTL) Dornbirn
<http://www.htldornbirn.at/>`_ and attracted several hundred attendees from
the so called "DACH" Region (Germany, Austria and Switzerland).

The schedule consisted of more than 20 separate lectures (presented in German),
that were held in three parallel tracks.

.. TEASER_END

Additionally, there was an opportunity to submit 10-minute lightning talks
that were held between 1pm-2pm.

I decided to give a spontaneuous lightning talk about the Sync & Share tool
`Syncthing <https://syncthing.net/>`_, as I was chatting about it with an
attendee during one of the crypto related sessions. I really like SyncThing as
a free alternative to BitTorrent Sync. I did not prepare any slides in
advance, so I just scribbled down a few bullet points on my Nexus 7 tablet.
Interestingly, nobody in the audience had heard about SyncThing before, so it
was nice to give them some inspiration.

My `talk about openATTIC
<http://www.linuxday.at/flexibles-storage-management-unter-linux-mit-openattic>`_
took place at 3pm in the biggest conference room 3. By my estimate, there were
about 25-30 attendees. I forgot to bring my wireless presenter, but
fortunately `Dirk Deimeke <http://www.linuxday.at/dirk-deimeke>`_ (another
speaker) was kind enough to lend me his. As I prefer to roam the stage during
presentations, I appreciated that I was not bound to the laptop for advancing
my slides. The presentation went quite well and there were some interesting
questions at the end. Hopefully some of the attendees will give openATTIC a
try and show up on our mailing list or #openattic IRC channel to share their
impressions!

The rest of the day I attended `other sessions
<http://www.linuxday.at/vortraege/2015>`_ that sparked my interest, here are
my impressions and takeaways:

`Kommunizieren mit Privatsphäre
<http://fileshare.lugv.at/public/vortraege2015/ld-profi/sichere_kommunikation.pdf>`_
by Manuel Wiesinger: the session abstract sounded interesting, but the
delivery left me somewhat disappointed. Because of lots of technical issues
related to getting his screen output directed to the projector, Manuel only
made it through a subset of his presentation. It was somewhat disheartening to
realize that connecting Linux laptops to projectors can still be an issue in
2015.

`Einführung in OpenPGP - Schlüssel, Vertrauen und das Web of Trust
<http://fileshare.lugv.at/public/vortraege2015/ld-profi/presentation-jens-erat-openpgp.pdf>`_
by Jens Erat provided a well-rounded introduction into PGP and the principles
behind it (encryption methods, web of trust). He concluded the presentation
with a list of useful tools and utilities. Unfortunately I did not get around
to get his take on `keybase.io <https://keybase.io>`_, which in my opinion is
an ideal alternative to traditional key signing parties.

`Zeit- und Selbstmanagement
<http://fileshare.lugv.at/public/vortraege2015/ld-business/zeit-_und_selbstmanagement_dornbirn.pdf>`_
by Dirk Deimeke: while it was a rather "soft" topic for such an event, Dirk
provided a very nice collection of useful hints and suggestions on how to
improve one's time management and self-organizational skills. Not
surprisingly, smartphones were identified as a key distraction factor and the
question was raised if they are more of a productivity killer than a useful
tool. Some attendees also shared their experiences and best practices, which
turned this session into a very insightful conversation. Dirk finished his
talk with an introduction to several Open Source tools that might be useful in
this context, but in the end it's usually up to an individual to find the best
method and tools to get organized and structured.

`Systemd - ein Init-System für Linux
<http://fileshare.lugv.at/public/vortraege2015/ld-profi/systemd.zip>`_ -
Stefan G. Weichinger: Stefan gave an excellent overview over the features and
functionality provided by `systemd
<http://freedesktop.org/wiki/Software/systemd/>`_. Unfortunately, he could not
finish his presentation as there were many questions and comments by the
attendees which lead to some intensive and lengthy discussions about the pros
and cons of systemd. This topic obviously still polarizes the Linux community,
with strong supporters in both camps. At this point I would have liked the
speaker or moderator to jump in to postpone the ongoing discussions until
after the presentation. Nevertheless, this talk was quite informative and I
learned a few more things about systemd that I wasn't aware of before.

`TDD - Test gestützte Programmierung mit Python 3
<http://fileshare.lugv.at/public/vortraege2015/ld-profi/Vortrag_TDD_Test_gest%C3%BCtzte_Programmierung/>`_
by Paul Jaros. In this talk, Paul gave a gentle introduction into the topic of
"Unit Testing", using simple Python code examples. He explained why automated
tests are important and why they need to be simple to create and quick to
perform.  He also outlined many common problems that can occur during
automated testing and finally introduced Python's `PyUnit
<https://docs.python.org/2/library/unittest.html>`_ unit testing framework as
the solution. I especially liked how a developer can use ``mock`` to emulate
the behaviour of existing functions or objects, e.g. to simulate writing to a
file. Paul finished his session relatively quickly, so another participant
offered to give a quick lightning talk about the `pytest
<http://pytest.org/>`_ testing tool as an alternative, which seemed to spur
more interest by the attendees. PyUnit's syntax closely resembles the unit
test framework used by Java and tests tend to become quite lenghty and
complicated because of that. Pytest convinved me with its simplicity and
flexibility, which was demonstrated live by several examples.

`Ein Bild sagt mehr als 1000 Zahlen
<http://fileshare.lugv.at/public/vortraege2015/ld-profi/linuxday-2015-gnuplot-slides.pdf>`_
by Harald König. Harald is a long-time guest speaker at LinuxDay, and his
presentations are usually the closing highlight of the day. After a short
basic introduction to `Gnuplot <http://www.gnuplot.info/>`_, the majority of
his session consisted of a live hacking session, in which Harald created many
graphs of increasing complexity, to introduce the usage of the Gnuplot command
line. It was quite impressive to see, how Gnuplot is capable of quickly
converting otherwise "dry" numbers into nice looking visuals with very little
effort.

The LinuxDay concluded with a joint dinner. Titled "Käsknöpfleparty", this
event traditionally takes place in the restaurant "Bierlokal" in the city
center of Dornbirn. The food was delicious and many more hours were spent with
interesting discussions and conversations about Linux, Open Source an many
other topics.

I truly enjoyed my stay, learned a lot and met many interesting people. So
even if the travel from Hamburg to Dornbirn was somewhat of an ordeal, it was
worth it! I look forward to next year.
