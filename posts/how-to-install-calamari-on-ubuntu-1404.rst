.. title: How to install Calamari on Ubuntu 14.04
.. slug: get-calamari-running-on-ubuntu-1404
.. date: 2016-02-05 11:03:52 UTC+01:00
.. tags: ceph, calamari, install, ubuntu, howto, opensource
.. category: 
.. link: 
.. description: How to install Calamari on Ubuntu 14.04
.. type: text
.. author: Tatjana Dehler

The current **Calamari version 1.3.1.** requires **SaltStack version 2014-7**.
Unfortunately the packages for version 2014-x are no longer available from the
official SaltStack repository.

Here is how to install Calamari and SaltStack 2014.7 on Ubuntu 14.04:

.. TEASER_END

#. Add the Calamari repository and the key::

	echo "deb http://download.ceph.com/calamari/1.3.1/ubuntu/trusty/ trusty main" > /etc/apt/sources.list.d/calamari.list
	wget --quiet -O - http://download.ceph.com/keys/release.asc | sudo apt-key add -

#. Add the SaltStack 2014-7 PPA::

	sudo add-apt-repository ppa:saltstack/salt2014-7

#. Update the source lists::

	sudo apt-get update

#. Install SaltStack master and minion::

	sudo apt-get install salt-master
	sudo apt-get install salt-minion

#. Install Calamari server and clients::

	sudo apt-get install calamari-server
	sudo apt-get install calamari-clients

#. Initialize Calamari::

	sudo calamari-ctl initialize

You can open the gui by calling your Calamari server in your web browser now.

If you receive an "Internal Server Error" it might be because some logfiles
are not accessible. This a known issue and can be found in the `ceph issue
tracker <http://tracker.ceph.com/issues/14179>`_. Try
``tailf /var/log/calamari/calamari.log`` and reload your browser's page.
If you finde something like that in your logfile::

	2016-02-05 06:27:25,626 - ERROR - django.request Internal Server Error: /
	Traceback (most recent call last):
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/core/handlers/base.py", line 103, in get_response
	    resolver_match = resolver.resolve(request.path_info)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/core/urlresolvers.py", line 319, in resolve
	    for pattern in self.url_patterns:
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/core/urlresolvers.py", line 347, in url_patterns
	    patterns = getattr(self.urlconf_module, "urlpatterns", self.urlconf_module)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/core/urlresolvers.py", line 342, in urlconf_module
	    self._urlconf_module = import_module(self.urlconf_name)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/utils/importlib.py", line 35, in import_module
	    __import__(name)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/calamari_web-0.1-py2.7.egg/calamari_web/urls.py", line 20, in <module>
	    url(r'^api/v1/', include('calamari_rest.urls.v1')),
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/conf/urls/__init__.py", line 25, in include
	    urlconf_module = import_module(urlconf_module)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/django/utils/importlib.py", line 35, in import_module
	    __import__(name)
	  File "/opt/calamari/venv/lib/python2.7/site-packages/calamari_rest_api-0.1-py2.7.egg/calamari_rest/urls/v1.py", line 3, in <module>
	    import calamari_rest.views.v1
	  File "/opt/calamari/venv/lib/python2.7/site-packages/calamari_rest_api-0.1-py2.7.egg/calamari_rest/views/v1.py", line 38, in <module>
	    from calamari_rest.views.server_metadata import get_local_grains
	  File "/opt/calamari/venv/lib/python2.7/site-packages/calamari_rest_api-0.1-py2.7.egg/calamari_rest/views/server_metadata.py", line 11, in <module>
	    from calamari_common.salt_wrapper import master_config, _create_loader, client_config, MasterPillarUtil
	  File "/opt/calamari/venv/lib/python2.7/site-packages/calamari_common-0.1-py2.7.egg/calamari_common/salt_wrapper.py", line 21, in <module>
	    handler = logging.FileHandler(config.get('cthulhu', 'log_path'))
	  File "/usr/lib/python2.7/logging/__init__.py", line 903, in __init__
	    StreamHandler.__init__(self, self._open())
	  File "/usr/lib/python2.7/logging/__init__.py", line 928, in _open
	    stream = open(self.baseFilename, self.mode)
	IOError: [Errno 13] Permission denied: '/var/log/calamari/cthulhu.log'

This issue might be solved by setting the right permissions to the log
directory::

	sudo chown www-data:www-data -R /var/log/calamari/

After another ``sudo calamari-ctl initialize`` the Calamari gui should be
visible now.

