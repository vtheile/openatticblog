.. title: Snapshot Python script with authtoken
.. slug: snapshot-python-script-with-authtoken
.. date: 2015-12-11 10:13:57 UTC+01:00
.. tags: authtoken, python, cronjob, script 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today i would like to show you, how you can easily create a Python script with openATTIC for automatic snapshot creation.

First of all we need a little Python script which can create snapshots for the specific openATTIC volume. This is really easy, because you can
use the "API Recorder" within openATTIC. You just have to select an existing volume, click the record button (the little record symbol on the right topbar)
and create a snapshot of that volume. Afterwards, you click the stop button (the record button switches into a stop button after you clicked it) and you will get
a working Python script, containing the actual API calls. 

.. TEASER_END

Example::
	
	#!/usr/bin/env python
	import requests
	import json
	auth = ("username", "password") # edit username and password

	headers = {'content-type': 'application/json'}

	### recorded command 1
	data=json.dumps({
	    "volumeId": 615,
	    "name": "2015-12-11-10-17-49",
	    "megs": 4577
	    })
	requests.post("http://openattic.yourdomain.com/openattic/api/volumes/615/snapshots", auth=auth, data=data, headers=headers)

The only thing you have to change is the username/password and the name of the snapshot. Thats it.

If you don't want to use username/password in plaintext inside the Python script, you can use the openATTIC authtoken. To obtain your specific token you have to insert the following commands::
	
	oaconfig dbshell
	select * from authtoken_auth;

Example Output::

	$ oaconfig dbshell
	psql (9.4.2)
	Type "help" for help.

	openattic=# select * from auth
	auth_group                         auth_permission_id_seq             auth_user_id_seq
	auth_group_id_seq                  authtoken_token                    auth_user_user_permissions
	auth_group_permissions             auth_user                          auth_user_user_permissions_id_seq
	auth_group_permissions_id_seq      auth_user_groups                   
	auth_permission                    auth_user_groups_id_seq            
	openattic=# select * from authtoken_token ;
        key                    |            created            | user_id 
        -------------------------------------+-------------------------------+---------
	 db2b68dd487bdfd24628ee07b814116acb2a228f | 2015-12-08 11:14:44.551939+01 |       1
	(1 row)

Now you can change the authentication from user/pass to token. I also insert the current date and time for snapshotname creation::

	#!/usr/bin/env python
	import requests
	import json
	import datetime
	token = "db2b68dd487bdfd24628ee07b814116acb2a228f"
	
	headers = {'content-type': 'application/json',
	           "Authorization": "Token %s" %token}
	
	currentime = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
	
	### recorded command 1
	data=json.dumps({
	        "volumeId": 615,
	        "name": currentime,
	        "megs": 4577
					                   })
	requests.post("http://openattic.yourdomain.com/openattic/api/volumes/615/snapshots", data=data, headers=headers)

Thats it. 
