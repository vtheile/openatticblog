.. title: openATTIC 2.0.12 beta has been released
.. slug: openattic-2.0.12-beta-has-been-released
.. date: 2016-06-24 14:45:13 UTC+02:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.12 beta release
.. type: text
.. author: Kai Wagner

openATTIC 2.0.12 beta has been released

We're happy to announce the availability of openATTIC version 2.0.12.

Some highlight of this release include some UI and workflow improvements on the host management page as well as improved input validation for FibreChannel WWNs. The WebUI now uses the user's web browser's local storage to keep track of personal UI settings like the sort order/criteria for data tables.

Also new in this release is a new Ceph management page that lists all existing RADOS block devices (RBDs) and their details on the selected cluster. The backend and REST API has received a lot of new Ceph management functionality - please see the `openATTIC Ceph REST API overview <https://wiki.openattic.org/display/OP/openATTIC+Ceph+REST+API+overview>`_ on the openATTIC Wiki for details. We also added initial Nagios/Icinga monitoring support for keeping track of a Ceph Cluster's overall health status and storing it in RRD files using PNP4Nagios. This functionality provides the foundation for the upcoming Ceph status dashboard, which will also display historic cluster performance data (:issue:`OP-85`). If you have troubles with updating your current development system you may consider the following topic on our Google Group `"Heads-up: Nagios-related configuration changes and development configurations" <https://groups.google.com/forum/#!topic/openattic-users/b5jAqyJWpHs>`_.

.. TEASER_END

To download and install openATTIC, please follow the `installation instructions <http://docs.openattic.org/2.0/install_guides/index.html>`_ in the documentation.

Please note that 2.0.12 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special thanks to Christian Eichelmann from the `ceph-dash project <https://github.com/Crapworks/ceph-dash>`_ for giving us permission to use parts of his Ceph monitoring plugin, the folks from SUSE and Luke Jing Yuan for their continued feedback and support.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.12:

* Backend: Added possiblility to create erasure-coded Ceph pools in the  REST API (:issue:`OP-546`)
* Backend: Added API call for creating new Ceph pools (:issue:`OP-1024`)
* Backend: Added modifying requests to Ceph pools (:issue:`OP-1170`, :issue:`OP-1172`)
* Backend: Added Ceph Pool snapshots to the REST API (:issue:`OP-1242`)
* Backend: Added support for Ceph cache tiering (:issue:`OP-1184`)
* Backend: Added field 'host' to Disks REST API resource (:issue:`OP-1195`)
* Backend: Added API call to activate and deactivate Ceph OSDs (:issue:`OP-1212`)
* Backend: Added Ceph RBD REST Collection (:issue:`OP-1214`)
* Backend: Added a Nagios plugin to monitor basic performance data of a Ceph cluster (:issue:`OP-1222`) (thanks to Christian Eichelmann for giving us the permission to integrate a part of his check-ceph-dash implementation)
* Backend: Added a basic infrastructure to create Nagios service definitions for known Ceph clusters (:issue:`OP-1235`)
* Backend: Added CephFS REST Collection (:issue:`OP-1245`)
* Backend: Cleaned up 'NAGIOS_SERVICES_CFG_PATH' backend setting. This setting will now be defined in the correct config files (openattic, openattic.RedHat, openattic.SUSE) only - no other duplicates or default values (:issue:`OP-1247`)
* Backend: Nagios services for Ceph clusters could now be created by 'oaconfig install' (:issue:`OP-1261`)
* Backend: Fixed listing of OSDs. Thanks to Luke Jing Yuan (:issue:`OP-1266`)
* Backend: Add functionality to remove existing Ceph cluster Nagios configuration files (:issue:`OP-1276`)
* Backend: Restarting the Nagios service is separated from the configuration creation now (:issue:`OP-1280`)
* Backend: Fixed internal server error in `/api/volumes`, if an `lvm` snapshot doesn't have a usage in percent (:issue:`OP-1111`)
* WebUI: Added WWN validation (:issue:`OP-475`)
* WebUI: Amend host views to better fit in in openATTIC (:issue:`OP-479`)
* WebUI: Save personal settings of each data table (:issue:`OP-1134`)
* WebUI: Renamed wizard "Raw Block Storage" to "iSCSI/FC target" (:issue:`OP-1151`)
* WebUI: Datatable shows a message if it is empty (:issue:`OP-1193`)
* WebUI: Create the ceph rbd module (:issue:`OP-1230`)
* WebUI: The Ceph Pool list is sortable now. Furthermore the columns cluster and  size have been removed (:issue:`OP-1237`)
* WebUI/QA: adapted helper function to new wizard title (:issue:`OP-1151`, :issue:`OP-1197`)
* WebUI/QA: adapted wizard test cases to new wizard title (:issue:`OP-1151`, :issue:`OP-1197`)
* WebUI/QA: Test the local storage capabilities of data tables (:issue:`OP-1202`)
* WebUI/QA: Test suite for the new ceph rbd module (:issue:`OP-1230`)
* WebUI/QA: Amend host tests for :issue:`OP-479` (:issue:`OP-1232`)
* WebUI/QA: Added WWN validation tests (:issue:`OP-1278`)
* Packaging: Adapted the build scripts to be able to create tarballs from  different sources and revisions (:issue:`OP-1118`)
* Packaging: Amended the create arguments of make_dist.py from 'stable' and  'unstable' to 'release' and 'snapshot' (:issue:`OP-1233`)
* Packaging: Amended the build scripts to not require existing mercurial tags anymore (:issue:`OP-1234`)
* Packaging: Changed dependency of the 'openattic-gui' package from 'openattic-  base' to 'openattic' (:issue:`OP-1137`)
* Packaging: Updated the dependencies on `ceph-common` to be of version 10.0.0  (aka "Jewel") or higher (:issue:`OP-1268`)
