.. title: Slidecast: Ceph and Storage Management with openATTIC
.. slug: slidecast-ceph-and-storage-management-with-openattic
.. date: 2016-07-21 12:54:13 UTC+02:00
.. tags: video, presentation, youtube, ceph, management, development
.. category:
.. link:
.. description: Announcing a new YouTube video about openATTIC 2.0 features
.. type: text
.. author: Lenz Grimmer

As many of the videos on the `openATTIC YouTube channel
<https://www.youtube.com/user/openATTICManagement>`_ are quite dated, I
recently started dabbling around with creating screen casts and slide casts,
so we can work on providing more up to date content about openATTIC there.

The first public result is a slide cast that gives a quick overview about
openATTIC 2.0, it's current features, ongoing development and future plans:

.. youtube:: Ooh-tjSVmNs

I hope you like it!
