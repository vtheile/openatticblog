.. title: Low hanging fruit tasks
.. slug: low-hanging-fruit-tasks
.. date: 2016-07-13 17:30:34 UTC+02:00
.. tags: opensource, contributing, development
.. category:
.. link: 
.. description: Introducing the list of simple tasks for new contributors
.. type: text
.. author: Lenz Grimmer

Becoming familar with an open source project's code base can be quite
overwhelming, especially for new developers.

While we tried our best to document the steps required for `setting up a
development environment
<http://docs.openattic.org/2.0/developer_docs/setup_howto.html>`_ and our
`contribution process
<http://docs.openattic.org/2.0/developer_docs/contribute.html>`_, the next
question usually is "where do I start?".

In the past days, we went through our `Jira bug tracker
<https://tracker.openattic.org>`_ and tried to identify a number of relatively
easy tasks that would be suitable for new developers to become familar with
the code base and processes involved. We've collected them on a dedicated Wiki
page titled `Low hanging fruit tasks
<https://wiki.openattic.org/display/OP/Low+hanging+fruit+tasks>`_.

These should be fairly simple and isolated tasks, so they would be ideal for
someone to dip his toes into the water.

If you're looking for something to hone your skills and expertise, these might
be a good start. If you're interested on working on any of these, `please let
us know <http://openattic.org/get-involved.html>`_!
