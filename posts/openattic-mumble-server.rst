.. title: openATTIC Mumble Server
.. slug: openattic-mumble-server
.. date: 2016-03-07 21:10:57 UTC+01:00
.. tags: mumble server installation howto 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

`Mumble <https://wiki.mumble.info/wiki/Main_Page/>`_ is a free open source voice communication software. It offers high quality sound, multi-platform support and ecrypted voice communication. The installation of Mumble is really easy, but when you want to include your own ssl certificate it starts to get complicated. 

Our Mumble server is based on Ubuntu 14.04.03 Trusty. First of all we have to start with the installation of the Mumble server.

.. TEASER_END

``Hint: First i used the`` `snapshot <https://launchpad.net/~mumble/+archive/ubuntu/snapshot/>`_ ``from github with the 1.3.0.x release, but i did not make it pass the following error``::

	1 => <1:(-1)> New connection: XXX.XXX.XXX.XXX:XXXXX
	1 => <1:(-1)> SSL Error: No certificates could be verified
	1 => <1:(-1)> Connection closed: [-1]

Install the mumble-server package::

	$ apt-get install mumble-server

Now configure the SuperUser and autostart::

	$ dpkg-reconfigure mumble-server

Optional: Change your hostname, welcome text, server password and other options in ``/etc/murmur.ini`` config.
	
**Our Mumble server is password protected. You find the URL and password at the end of this blog post.**

Restart your server:: 

	$ service mumble-server restart

and actually we are finished and can start using our mumble server, but we still want to use our private ssl certificate. 

If you don't want to buy an expensive SSL-Cert you can use one of the free certificate authoritys like `Let's Encrypt <https://letsencrypt.org//>`_ or `StartSSL <https://www.startssl.com//>`_. Our SSL-Cert is a wildcard cert from `RapidSSL <https://www.rapidssl.com/>`_ for openattic.org. 

First, copy your CA file to ``/usr/local/share/ca-certificates/``

then update the CA store::

	$ update-ca-certificates

That's all. You should get something similiar::

	Updating certificates in /etc/ssl/certs... 1 added, 0 removed; done.
	Running hooks in /etc/ca-certificates/update.d....
	Adding debian:openattic_org.pem
	done.
	done.

Now copy your ca, crt and key file to ``/etc/`` (you can create and use any path you want to, but its easier to use the same folder as mumble server does).

Add the following to your ``/etc/murmur.ini``::

	sslCert=openattic_org.crt
	sslKey=openattic_org.key
	sslCa=openattic_org.ca

When you're done you have to wipe the old certificate::

	$ murmurd -wipessl

Now you can start mumble server.::

	$ service mumble-server start

Have fun!

.. raw:: html

       <a id="spoilerLink" data-show-text="Show openATTIC Mumble server url and password" data-hide-text="Hide openATTIC Mubmle server url and password">Show openATTIC Mumble server url and password</a>·
       <div id="spoilerBody" style="display: none">
       <p>

.. code:: bash

	URL: mumble.openattic.org
	Password: openattic-community

.. raw:: html

       </div>
