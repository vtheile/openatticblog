.. title: openATTIC 2.0.11 beta has been released
.. slug: openattic-2.0.11-beta-has-been-released
.. date: 2016-05-20 15:36:13 UTC+02:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.11 beta release
.. type: text
.. author: Stephan Müller

We're happy to announce the release of openATTIC version 2.0.11. One of the highlights of this release is, that we've made further progress regarding the `Ceph-related development <https://wiki.openattic.org/display/OP/openATTIC+Ceph+Management+Roadmap+and+Implementation+Plan>`_:

In addition to the pool list of our last release, it is now possible to list placement groups and view the Ceph cluster status. 
Besides the ability to display a list of OSDs, we have added a drop-down menu to be able to select the current Ceph cluster in the WebUI on all Ceph pages. Once a cluster has been selected, the GUI will keep the selection when switching between Ceph-pages.

Furthermore, the functionality to add a volume in the GUI has been extended. It now offers **all** supported volume types to the user, depending on the selected pool.
When selecting a volume type, the WebUI will offer a description text which also links to an external article with additional information about the selected type. This should make the decision easier for the user when considering which one would be best for the user's purpose.

As requested by a customer, we have added the possibility to openATTIC to grow the size of Btrfs pools.

Also, we fixed a bunch of bugs, extended and updated the documentation and took care of quality assurance as well.

.. TEASER_END

Please note that 2.0.11 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special thanks to the folks from SUSE for their feedback and support.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.11:

* WebUI: Extended the pool selection box by the hostname of the pool (:issue:`OP-973`)
* WebUI: Extended the volume creation form by all supported filesystems with description (:issue:`OP-988`)
* WebUI: Snapshot size isn't asked for if creating a snapshot on a zfs or btrfs (:issue:`OP-1088`)
* WebUI: Added dropdown menu to the Ceph pool list to select a cluster (:issue:`OP-1104`)
* WebUI: Saved selected Ceph cluster between page changes (:issue:`OP-1131`)
* WebUI: Added Ceph OSD data table (:issue:`OP-1144`)
* WebUI: Hid snapshot columns with wrong data on a zfs or btrfs (:issue:`OP-1162`)
* WebUI: Linked the type description with a Wikipedia article (:issue:`OP-1189`)
* WebUI/QA: Added test cases for datatable (:issue:`OP-818`)
* WebUI/QA: Added test case for hostname in pool dropdown (:issue:`OP-973`)
* WebUI/QA: Tested for the hostnames of the pools in the volume form (:issue:`OP-1059`)
* WebUI/QA: Added tests which check the correct description for each volume type (:issue:`OP-1159`)
* WebUI/QA: Tested to verify that snapshot size is hidden while creating a snapshot on a zfs or btrfs (:issue:`OP-1162`)
* WebUI/QA: Added E2E test cases for Ceph OSDs Panel (:issue:`OP-1176`)
* WebUI/QA: Removed obsolete E2E tests due to :issue:`OP-597` (OP-1185)
* WebUI/QA: Adapted datatable tests due to :issue:`OP-597` (OP-1186)
* WebUI/QA: Speeded up multiple volume deletion test (:issue:`OP-1187`)
* WebUI/QA: Refactored the multiple sequential volume creation and deletion test (:issue:`OP-1190`)
* Backend: Disks (falsely displayed) are removed from the volume management (:issue:`OP-597`)
* Backend: Removed simplejson dependency (:issue:`OP-783`)
* Backend: Fixed "close_connection is superseded by close_old_connections.close_connection()" RemovedInDjango18Warning (:issue:`OP-809`)
* Backend: Exported Ceph OSD details in the REST API as `/api/ceph/<fsid>/osds` (:issue:`OP-1035`)
* Backend: Added Btrfs grow functionality (:issue:`OP-1093`)
* Backend: Added two missing parameters 'delete' and 'id', added in :issue:`OP-736`, to dbus-send Samba writeconf calls (OP-1113)
* Backend: Fixed compatibility issue with Ceph Jewel when running `oaconfig install` (:issue:`OP-1079`) (thanks to Abhishek Lekshmanan for the patch)
* Backend: Fixed warnings from the Ceph module when calling `oaconfig install` (:issue:`OP-1121`)
* Backend: Added licence headers for nodb files (:issue:`OP-1125`)
* Backend: Switched to Django logging functionality for enhanced logging quality (:issue:`OP-1126`)
* Backend: Added searching, filtering and ordering to nested viewsets (:issue:`OP-1090`, OP-1129, OP-1142)
* Backend: Fixed crash caused by unexpected output of `ceph osd dump` in the `hit_set_params` field (:issue:`OP-1130`)
* Backend: Provided a detail page for Ceph pools via the REST API (:issue:`OP-1071`)
* Backend: Revised the Ceph pool listing (:issue:`OP-1135`)
* Backend: Added Ceph status values to Ceph cluster REST API resource (:issue:`OP-1146`)
* Backend: Added Ceph Placement Groups to the REST API (:issue:`OP-1148`)
* Backend: Enabled logging for the Nagios and Ceph module by default (thanks to Eric Jackson)
* Backend: Let the Ceph module check for keyring permissions and log permission problems (thanks to Eric Jackson)
* Backend/Installation: Fixed update_disks() method that is called during "oaconfig install", so that all hard disks attached to a system are being found, and not just the first 16 disks (thanks to IRC user "SNMPGuy" for reporting this) (:issue:`OP-1136`)
* Installation: Enabled logrotate configuration for openattic.log (:issue:`OP-1124`)
* Installation: Added instructions to create `openattic.log` with correct permissions when installing packages (:issue:`OP-1132`)
* Documentation: Added styleguide for E2E tests (:issue:`OP-1015`)
* Documentation: Added note about extra package which needs to be installed when using Ubuntu 14.04 LTS in order to get lio-utils working (:issue:`OP-1105`)
* Documentation: Created dedicated document "Post Installation" that explains common tasks to be performed after installing openATTIC, e.g. changing the openattic user's default password (:issue:`OP-1038`)
* Documentation: Moved Ceph configuration and how to install additional openATTIC modules from the Admin Guide into the post-installation document. Moved "Getting started" into a separate document. Moved "Hardware Recommendations" and "Storage Recommendations" into a separate document
* Documentation: Added chapter on how to configure a ZFS zpool to the "Basic Storage Configuration" chapter (:issue:`OP-711`)
* Documentation: Added basic instructions on how to install oA on SUSE Linux to the Installation chapter (:issue:`OP-986`)
* Documentation: Added documentation on how to enable alternative authentication methods and how to perform a domain join to a new chapter "Configuring Authentication and Single Sign-On" (:issue:`OP-1044`)
* Documentation: Added chapter about how to work on the documentation to the Developer Docs
